const swaggerJSDoc = require("swagger-jsdoc");

const swaggerDefinition = {
  info: {
    title: "Kilimanjaro",
    description: "k90 API Gateway REST API Documentation",
    termsOfService: "https://formelo.com",
    contact: {
      name: "k90 Support",
      url: "formelo.com",
      email: "kelzvictoria@gmail.com",
    },

    license: {
      name: "Test License",
      url: "https://license.foo.com",
      version: "1.0.0",
    },

    servers: {
      url: "https://devlab.formelo.com/api/",
      description: "Dev Server",
    },

    security: {
      formeloAuth: [],
    },

  },

  host: "devlab.formelo.com", // the host or url of the app
  basePath: "/api", // the basepath of your endpoint
  schemes: ['https']
};

// options for the swagger docs
const options = {
  // import swaggerDefinitions
  swaggerDefinition,
  // path to the API docs
  //apis: ["./docs/**/*.yaml"],
  apis: ["./index.yaml" ,"./schemas/**/*.yaml", "./docs/**/*.yaml", "./requestBodies/**/*.yaml"],
};
// initialize swagger-jsdoc
module.exports = swaggerJSDoc(options);
